#include <QTRSensors.h>
#include <AFMotor.h>

AF_DCMotor left_motor(1, MOTOR12_64KHZ); // create motor #1, 64KHz pwm
AF_DCMotor right_motor(2, MOTOR12_64KHZ); // create motor #2, 64KHz pwm

QTRSensors qtr;

const uint8_t SensorCount = 8;
uint16_t sensorValues[SensorCount];

void setupSensors() {
  qtr.setTypeRC();
  qtr.setSensorPins((const uint8_t[]){31, 32, 33, 34, 35, 36, 37, 38}, SensorCount);
  qtr.setEmitterPin(30);

  Serial.println("Calirating sensors ... ");

  delay(500);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH); // turn on Arduino's LED to indicate we are in calibration mode

  // 2.5 ms RC read timeout (default) * 10 reads per calibrate() call
  // = ~25 ms per calibrate() call.
  // Call calibrate() 400 times to make calibration take about 10 seconds.
  for (uint16_t i = 0; i < 400; i++)
  {
    qtr.calibrate();
  }
  digitalWrite(LED_BUILTIN, LOW); // turn off Arduino's LED to indicate we are through with calibration

  // print the calibration minimum values measured when emitters were on
  
  Serial.println("Minimum values: ");
  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(qtr.calibrationOn.minimum[i]);
    Serial.print(' ');
  }
  Serial.println();

  // print the calibration maximum values measured when emitters were on
  Serial.println("Maximum values: ");
  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(qtr.calibrationOn.maximum[i]);
    Serial.print(' ');
  }
  Serial.println();
  Serial.println();
  delay(5000);
}
int defaultSpeed = 75;

void setupMotors() {
  left_motor.run(RELEASE);
  right_motor.run(RELEASE);

  left_motor.setSpeed(defaultSpeed);
  right_motor.setSpeed(defaultSpeed);
}

void setup() {
  Serial.begin(9600);
  
  setupMotors();
  setupSensors(); 
  
  left_motor.run(BACKWARD);
  right_motor.run(BACKWARD);
}

void loop() {
  unsigned int sensors[8];
  int position = qtr.readLineBlack(sensors);
  int error = position - 3500;
 
  int m1Speed = defaultSpeed;
  int m2Speed = defaultSpeed;

  if (error < -500) {
    m1Speed = 0; 
  }
  if (error > 500) {
    m2Speed = 0; 
  }

  if (error < -1500) {
    m2Speed = defaultSpeed*2.0;
  }
  if (error > 1500) {
    m1Speed = defaultSpeed*2.0;
  }
 
  Serial.println("error: " + String(error) + " M1: " + String(m1Speed) + " M2: " + String(m2Speed));

  left_motor.setSpeed(m2Speed);
  right_motor.setSpeed(m1Speed);
}
