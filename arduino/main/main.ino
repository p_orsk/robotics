#include <AFMotor.h>
#include <NewPing.h>
#include <QTRSensors.h>
#include <Wire.h>
#include <Servo.h>
#include <LiquidCrystal_PCF8574.h>

#define TRIGGER_PIN 29
#define ECHO_PIN 28
#define MAX_DISTANCE 150

#define DISTANCE_THRESHOLD_CM 16
#define DISTANCE_CHECK_LIMIT 3

#define ROBOT_LENGTH 40

// Servo motor at the front
Servo front_motor;

// DC motors
AF_DCMotor left_motor(1, MOTOR12_64KHZ); // create motor #1, 64KHz pwm
AF_DCMotor right_motor(2, MOTOR12_64KHZ); // create motor #2, 64KHz pwm

// Ultrasonic sensor
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);
// Light sensors
QTRSensors qtr;
// LCD
//LiquidCrystal_PCF8574 lcd(0x27);

const uint8_t SensorCount = 8;
uint16_t sensorValues[SensorCount];

const short set_speed_command = 0x0;
const short set_angle_command = 0x1;
const short set_direction_command = 0x2;
const short rotate_in_place_command = 0x3;
const short set_robot_mode_command = 0x4;
const short calibrate_light_sensors = 0x5;
const short start_line_following = 0x6;
const short stop_line_following = 0x7;
const short avoid_command = 0x8;
//const short write_to_lcd = 0x9;

const short manual_mode = 0x0;
const short line_follower_mode = 0x1;

// between 0 - 180
const short servo_middle_position = 90;
const short servo_left_position = 180;
const short servo_right_position = 0;

short current_mode = manual_mode;
short current_speed = 0; // 0 - 255
short current_direction = RELEASE; // FORWARD or BACKWARD or RELEASE
short current_angle = 0; // 0 - straight; -100 - left; 100 - right
short follow_line = false;
unsigned int distance = 0;
unsigned int lastDistances[10];
short avoid_left = false;
short avoid_right = false;

//String message = "";
//int loopCount = 0;

void setMotorsSpeed(short speed, short angle) {
  int left_motor_speed = speed;
  int right_motor_speed = speed;
  
  if (angle > 0) {
    right_motor_speed = (int) ((1.0 - angle / 100.0) * speed);
  } else {
    left_motor_speed = (int) ((1.0 - abs(angle) / 100.0) * speed);
  }

  left_motor.setSpeed(left_motor_speed);
  right_motor.setSpeed(right_motor_speed);
}

void setMotorsDirection(short direction) {
  left_motor.run(direction);
  right_motor.run(direction);
}

void setupLightSensors() {
  qtr.setTypeRC();
  qtr.setSensorPins((const uint8_t[]){31, 32, 33, 34, 35, 36, 37, 38}, SensorCount);
  qtr.setEmitterPin(30);
}

void calibrateLightSensors() {
  for (uint16_t i = 0; i < 300; i++)
  {
    qtr.calibrate();
  }

  // print the calibration minimum values measured when emitters were on
  
  Serial.println("Minimum values: ");
  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(qtr.calibrationOn.minimum[i]);
    Serial.print(' ');
  }
  Serial.println();

  // print the calibration maximum values measured when emitters were on
  Serial.println("Maximum values: ");
  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(qtr.calibrationOn.maximum[i]);
    Serial.print(' ');
  }
  
  Serial.println();
}

/*void initLCD() {
  int error; 
  
  Wire.begin();
  Wire.beginTransmission(0x27); //Your LCD Address
  error = Wire.endTransmission();
  
  Serial.print("Error: ");
  Serial.print(error);
 
  if (error == 0) {
    Serial.println(": LCD found.");
 
  } else {
    Serial.println(": LCD not found.");
  }

  lcd.begin(16, 2);
  lcd.setBacklight(50);
  lcd.clear();
}*/

void setup() {
  // set up Serial library at 9600 bps
  Serial.begin(9600);

  // setup I2C communication
  Wire.begin(0x4); 
  Wire.onReceive(receiveEvent);
  Wire.onRequest(requestEvent);

  // setup servo and positionate the head
  front_motor.attach(10);
  front_motor.write(servo_middle_position);

  // set default speed
  setMotorsSpeed(current_speed, current_angle);

  // stop both motors
  setMotorsDirection(current_direction);

  // setup light sensors
  setupLightSensors();

  for (short i = 0; i < 10; ++i) {
    lastDistances[i] = 0;
  }

  //initLCD();
}

void requestEvent() {
  Wire.write(distance);
}

void setDirection(int value) {
  if (value == 2) {
    current_direction = FORWARD;
  } else if (value == 1) {
    current_direction = BACKWARD;
  } else if (value == 0) {
    current_direction = RELEASE;
  }

  setMotorsDirection(current_direction);
}

void rotateInPlace(int dir) {
  if (dir == 0) { // left
    left_motor.run(FORWARD);
    right_motor.run(BACKWARD);    
  } else if (dir == 1) { // right
    left_motor.run(BACKWARD);
    right_motor.run(FORWARD);   
  }
}

void avoidObstacle(int dir) {
  if (dir == 0) {
    avoid_left = true;
  } else {
    avoid_right = true;
  }
}

void handleCommandsInManualMode(int command, int value) {
  switch (command) {
    case set_speed_command: {
      Serial.println("New robot speed: " + String(value));
      current_speed = value;
      setMotorsSpeed(current_speed, current_angle);
      break;
    }

    case set_angle_command: {
      value = value - 100;
      Serial.println("New robot angle: " + String(value));
      current_angle = value;
      setMotorsSpeed(current_speed, current_angle);
      break;
    }

    case set_direction_command: {
      Serial.println("New robot direction: " + String(value));
      setDirection(value);
      break;
    }

    case rotate_in_place_command: {
      Serial.println("Rotation in place: " + String(value));
      rotateInPlace(value);
      break;
    }

    case avoid_command: {
      Serial.println("Avoid obstacle in direction: " + String(value));
      avoidObstacle(value);
      break;
    }

    default: break;
  }
}

void handleCommandsInLineFollowerMode(int command, int value) {
  switch(command) {
    case calibrate_light_sensors: {
      Serial.println("Calibrating light sensors ...");
      calibrateLightSensors();
      break;
    }

    case start_line_following: {
      setMotorsDirection(BACKWARD);
      follow_line = true;
      break;
    }

    case stop_line_following: {
      setMotorsDirection(RELEASE);
      follow_line = false;
      break;
    }
    default: break;
  }
}

void changeMode(int mode) {
  setMotorsDirection(RELEASE);
    
  if (mode == manual_mode) {
    Serial.println("Changed to manual mode");
    follow_line = false;
    current_speed = 0;
    left_motor.setSpeed(current_speed);
    right_motor.setSpeed(current_speed);
    current_mode = manual_mode;
  } else if (mode == line_follower_mode) {
    Serial.println("Changed to line following mode");
    current_mode = line_follower_mode;
  }
}

/*void showOnLCD(int first) {
  String tmp = "";
  tmp += (char) first;
  
  while(Wire.available())    // slave may send less than requested
  {
    char c = Wire.read();    // receive a byte as character
    tmp += c;         // print the character
  }

  Serial.println("New message to print: " + tmp);
  message = tmp;
}*/

void receiveEvent(int howMany)
{
  int command = Wire.read();
  int value = Wire.read();

  /*if (command == write_to_lcd) {
    showOnLCD(value);
  }*/

  if (avoid_left || avoid_right) return;

  if (command == set_robot_mode_command) {
    changeMode(value);
  } else if (current_mode == manual_mode) {
    handleCommandsInManualMode(command, value);
  } else if (current_mode == line_follower_mode) {
    handleCommandsInLineFollowerMode(command, value);
  }
}

int getDistance() {
  unsigned int distance = sonar.ping_cm();

  short limit = DISTANCE_CHECK_LIMIT;
  
  while (distance == 0 && limit > 0) {
    distance = sonar.ping_cm();

    limit--;
  }
  
  return distance;
}

/*int checkDistance(int servo_position) {
  front_motor.write(servo_position);
  delay(750);

  unsigned int distance = getDistance();
  
  front_motor.write(servo_middle_position);
  delay(750);

  return distance;
}*/

void backToSafeDistance() {
  setMotorsDirection(FORWARD);

  left_motor.setSpeed(90);
  right_motor.setSpeed(90);

  while(getDistance() < DISTANCE_THRESHOLD_CM);

  setMotorsDirection(RELEASE);

  left_motor.setSpeed(current_speed);
  right_motor.setSpeed(current_speed);
}

void checkDistanceAhead() {
  unsigned int current_distance = getDistance();
  unsigned int avgDistance = 0;
  
  for (short i = 1; i < 10; ++i) {
    lastDistances[i-1] = lastDistances[i];
    avgDistance += lastDistances[i];
  }
  
  lastDistances[9] = current_distance;
  avgDistance += current_distance;
  avgDistance /= 10;
  Serial.println("Front: " + String(avgDistance) + " cm.");
  
  if (avgDistance > 0 && avgDistance < DISTANCE_THRESHOLD_CM) {
    backToSafeDistance();
  }

  distance = avgDistance;
}

void rotate90Degree(int dir) {
  // Rotate in place 90 degree - L1500ms, R1400ms on carpet, - 1250ms on floor
  rotateInPlace(dir);
  left_motor.setSpeed(150);
  right_motor.setSpeed(150);
  
  if (dir == 1) {
    delay(1250);
  } else {
    delay(1250);
  }
  
  setMotorsDirection(RELEASE);
}



void avoid(int dir) {
  // Turn head left
  if (dir == 1) {
     front_motor.write(servo_left_position);
  } else {
     front_motor.write(servo_right_position);
  }

  delay(750);

  rotate90Degree(dir);

  // Going forward until no obstacle at the left side
  setMotorsDirection(BACKWARD);
  while(getDistance() < DISTANCE_THRESHOLD_CM * 3);

  // Turn back the head to middle position
  front_motor.write(servo_middle_position);

  // Go forward a bit (around the lenght of the robot)
  delay(1300);

  setMotorsDirection(RELEASE);

  if (dir == 1) {
     rotate90Degree(0);
  } else {
     rotate90Degree(1);
  }
  setMotorsDirection(BACKWARD);
  delay(1300);
  setMotorsDirection(RELEASE);
}

void loop() {
  if (current_mode == line_follower_mode && follow_line) {
    unsigned int sensors[8];
    int position = qtr.readLineBlack(sensors);
    int error = position - 3500;

    int defaultSpeed = 90;
    int m1Speed = defaultSpeed;
    int m2Speed = defaultSpeed;
  
    if (error < -500) {
      m1Speed = 0; 
    }
    if (error > 500) {
      m2Speed = 0; 
    }
  
    if (error < -1500) {
      m2Speed = defaultSpeed*2.0;
    }
    if (error > 1500) {
      m1Speed = defaultSpeed*2.0;
    }
   
    Serial.println("Error: " + String(error) + " | M1: " + String(m1Speed) + " | M2: " + String(m2Speed));
  
    left_motor.setSpeed(m2Speed);
    right_motor.setSpeed(m1Speed);
  }

  if (current_mode != line_follower_mode)
    checkDistanceAhead();

  if (avoid_right) {
    avoid_right = false;
    avoid(1);
  }

  if (avoid_left) {
    avoid_left = false;
    avoid(0);
  }

  /*if (loopCount == 50) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("IP: " + message);
    lcd.setCursor(0, 1);
    lcd.print("Distance: " + String(distance) + " cm");

    loopCount = 0;
  }*/

  //loopCount++;
}
