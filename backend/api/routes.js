let express = require("express");

let router = express.Router();

let c = require("./controllers");

// Manual control
router.route("/setSpeed").post(c.setRobotSpeed);
router.route("/setAngle").post(c.setRobotAngle);
router.route("/setDirection").post(c.setRobotDirection);
router.route("/rotateInPlace").post(c.rotateInPlace);
router.route("/avoid").post(c.avoid);

router.route("/setMode").post(c.setRobotMode);

// Line following mode
router.route("/calibrateLightSensors").post(c.calibrateLightSensors);
router.route("/startFollowing").post(c.startFollowing);
router.route("/stopFollowing").post(c.stopFollowing);

module.exports = router;
