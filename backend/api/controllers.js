let arduino = require("../arduino/arduino");

exports.setRobotSpeed = (req, res, next) => {
  if (
    typeof req.body.speed !== "undefined" &&
    req.body.speed >= 0 &&
    req.body.speed <= 255
  ) {
    arduino
      .setRobotSpeed(req.body.speed)
      .then(() => {
        res.status(200).json({ message: "OK" });
      })
      .catch(next);
  } else {
    res.status(400).json({
      message: "<speed> body paramter needs to be in the range of [0, 255]."
    });
  }
};

exports.setRobotAngle = (req, res, next) => {
  if (
    typeof req.body.angle !== "undefined" &&
    req.body.angle >= 0 &&
    req.body.angle <= 200
  ) {
    arduino
      .setRobotAngle(req.body.angle)
      .then(() => {
        res.status(200).json({ message: "OK" });
      })
      .catch(next);
  } else {
    res.status(400).json({
      message: "<angle> body paramter needs to be in the range of [0, 200]."
    });
  }
};

exports.setRobotDirection = (req, res, next) => {
  if (
    typeof req.body.direction !== "undefined" &&
    req.body.direction >= 0 &&
    req.body.direction <= 2
  ) {
    arduino
      .setRobotDirection(req.body.direction)
      .then(() => {
        res.status(200).json({ message: "OK" });
      })
      .catch(next);
  } else {
    res
      .status(400)
      .json({ message: "<direction> body paramter needs to be 0, 1 or 2." });
  }
};

exports.rotateInPlace = (req, res, next) => {
  if (
    typeof req.body.direction &&
    (req.body.direction === "left" || req.body.direction === "right")
  ) {
    arduino
      .rotateInPlace(req.body.direction)
      .then(() => {
        res.status(200).json({ message: "OK" });
      })
      .catch(next);
  } else {
    res.status(400).json({
      message: "<direction> body paramter needs to be left or right."
    });
  }
};

exports.setRobotMode = (req, res, next) => {
  if (
    typeof req.body.mode &&
    (req.body.mode === "manual" || req.body.mode === "line_follower")
  ) {
    arduino
      .setMode(req.body.mode)
      .then(() => {
        res.status(200).json({ message: "OK" });
      })
      .catch(next);
  } else {
    res.status(400).json({
      message: "<mode> body paramter needs to be manual or line_follower."
    });
  }
};

exports.calibrateLightSensors = (req, res, next) => {
  arduino
    .calibrateLightSensors()
    .then(() => {
      res.status(200).json({ message: "OK" });
    })
    .catch(next);
};

exports.startFollowing = (req, res, next) => {
  arduino
    .startFollowing()
    .then(() => {
      res.status(200).json({ message: "OK" });
    })
    .catch(next);
};

exports.stopFollowing = (req, res, next) => {
  arduino
    .stopFollowing()
    .then(() => {
      res.status(200).json({ message: "OK" });
    })
    .catch(next);
};

exports.avoid = (req, res, next) => {
  if (
    typeof req.body.mode &&
    (req.body.direction === "left" || req.body.direction === "right")
  ) {
    arduino
      .avoid(req.body.direction)
      .then(() => {
        res.status(200).json({ message: "OK" });
      })
      .catch(next);
  } else {
    res.status(400).json({
      message: "<direction> body paramter needs to be left or right."
    });
  }
};
