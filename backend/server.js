let express = require("express"),
  bodyParser = require("body-parser"),
  cors = require("cors"),
  morgan = require("morgan"),
  http = require("http"),
  socketIO = require("socket.io");

let port = process.env.PORT || 9000;

let app = express();
let server = http.createServer(app);
let io = socketIO(server);

app.use(morgan("tiny"));
app.use(cors());
app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.urlencoded({ extended: true }));

// Routes

let router = require("./api/routes");
app.use("/", router);

app.use(express.static('public'));

app.get('*', (_, res) => {
  res.sendFile('index.html', { root: path.join(__dirname, '/public') });
})

server.listen(port, () => {
  console.log(`HTTP API server is up and running under port ${port}.`);
});

let arduino = require("./arduino/arduino");
//Connect to arduinos
arduino.establishConnections(io);

module.exports = app;
