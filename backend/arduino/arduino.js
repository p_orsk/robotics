const i2c = require("i2c-bus"),
      ip = require("ip"),
      LCD = require('lcdi2c');

const SET_ROBOT_SPEED_CMD = 0x0;
const SET_ROBOT_ANGLE_CMD = 0x1;
const SET_ROBOT_DIRECTION_CMD = 0x2;
const ROTATE_IN_PLACE_CMD = 0x3;
const SET_ROBOT_MODE_CMD = 0x4;
const CALIBRATE_LIGHT_SENSORS_CMD = 0x5;
const START_LINE_FOLLOWING = 0x6;
const STOP_LINE_FOLLOWING = 0x7;
const AVOID_CMD = 0x8;
//const WRITE_TO_LCD_CMD = 0x9;

const MANUAL_MODE = 0x0;
const LINE_FOLLOWER_MODE = 0x1;

const ADDRESS = 0x4;
const COMMAND_LENGTH = 2;

let robot = null;
let lcd = new LCD(1, 0x27, 20, 4);

let count = 0;

exports.establishConnections = io => {
  robot = i2c.open(1, { forceAccess: true }, () => {
    console.log(`Connection is open to the Robot over I2C.`);

    // Reading distance periodically
    setInterval(() => readAndSendDistance(io), 1000);
  });
};

const readAndSendDistance = io => {
  const response = Buffer.alloc(1);

  robot.i2cRead(ADDRESS, response.length, response, () => {
    const distance = response.values().next()["value"];

    count++;
    if (count === 3) {
      updateLCD(distance);
      count = 0;
    }
    
    io.emit("distance", distance);
  });
};

const updateLCD = (distance) => {
  const address = ip.address();

  lcd.println('IP: ' + address, 1);
  lcd.println('Distance: ' + distance + "   ", 2);
}

// Speed should be in the range of 0 - 255
exports.setRobotSpeed = speed => {
  return sendMessageToRobot(SET_ROBOT_SPEED_CMD, speed);
};

// Angle should be in the range of 0 - +200
exports.setRobotAngle = angle => {
  return sendMessageToRobot(SET_ROBOT_ANGLE_CMD, angle);
};

// direction should be 0, 1 or 2
exports.setRobotDirection = direction => {
  return sendMessageToRobot(SET_ROBOT_DIRECTION_CMD, direction);
};

// direction should be left or right
exports.rotateInPlace = direction => {
  if (direction === "left") {
    return sendMessageToRobot(ROTATE_IN_PLACE_CMD, 0);
  } else if (direction === "right") {
    return sendMessageToRobot(ROTATE_IN_PLACE_CMD, 1);
  }
};

exports.setMode = mode => {
  if (mode === "manual") {
    return sendMessageToRobot(SET_ROBOT_MODE_CMD, MANUAL_MODE);
  } else if (mode === "line_follower") {
    return sendMessageToRobot(SET_ROBOT_MODE_CMD, LINE_FOLLOWER_MODE);
  }
};

exports.calibrateLightSensors = () => {
  return sendMessageToRobot(CALIBRATE_LIGHT_SENSORS_CMD, 0);
};

exports.startFollowing = () => {
  return sendMessageToRobot(START_LINE_FOLLOWING, 0);
};

exports.stopFollowing = () => {
  return sendMessageToRobot(STOP_LINE_FOLLOWING, 0);
};

exports.avoid = direction => {
  if (direction === "left") {
    return sendMessageToRobot(AVOID_CMD, 0);
  } else if (direction === "right") {
    return sendMessageToRobot(AVOID_CMD, 1);
  }
};

/*exports.sendToLCD = message => {
  const cmdBuffer = new Buffer.from([WRITE_TO_LCD_CMD]);
  const messageBuffer = new Buffer.from(message);

  const data = Buffer.concat([cmdBuffer, messageBuffer]);

  return new Promise((resolve, reject) => {
    robot.i2cWrite(ADDRESS, data.byteLength, data, error => {
      if (error) {
        reject();
        console.log("Error during sending message to Robot.");
      }
      resolve();
      console.log("Message sent to robot");
    });
  });
};*/

function sendMessageToRobot(command, value) {
  return new Promise((resolve, reject) => {
    robot.i2cWrite(
      ADDRESS,
      COMMAND_LENGTH,
      new Buffer.from([command, value]),
      error => {
        if (error) {
          reject();
          console.log("Error during sending message to Robot.");
        }
        resolve();
      }
    );
  });
}
