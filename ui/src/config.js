const getBackendUrl = () => {
    if (process.env.NODE_ENV === 'development') return  'http://192.168.43.107:9000'//'http://192.168.88.191:9000';

    return window.location.origin;
};

const config = {
    apiUrl: process.env.API_URL || getBackendUrl() + '/',
};

module.exports = config;
