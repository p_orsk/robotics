import React from 'react';
import { AppBar, Toolbar, Button, Divider, CssBaseline } from '@material-ui/core/';
import Control from './Control';

import api from '../api';
import LineFollow from './LineFollow';

const MANUAL_CONTROL = 'manual';
const LINE_FOLLOWER = 'line_follower';

class Main extends React.Component {
    constructor() {
        super();

        this.state = {
            mode: MANUAL_CONTROL,
        };
    }

    setManualMode() {
        this.setState({ mode: MANUAL_CONTROL });
        api.post('/setMode', { mode: MANUAL_CONTROL });
    }

    setLineFollowerMode() {
        this.setState({ mode: LINE_FOLLOWER });
        api.post('/setMode', { mode: LINE_FOLLOWER });
    }

    render() {
        return (
            <div style={{ flexGrow: 1 }}>
                <CssBaseline />
                <AppBar position="static" color="default">
                    <Toolbar>
                        <Button
                            onClick={() => this.setManualMode()}
                            style={{ color: this.state.mode === MANUAL_CONTROL ? '#3f51b5' : 'rgba(0, 0, 0, 0.87)' }}>
                            Manual control
                        </Button>
                        <Divider style={{ margin: 20 }}></Divider>
                        <Button
                            onClick={() => this.setLineFollowerMode()}
                            style={{ color: this.state.mode === LINE_FOLLOWER ? '#3f51b5' : 'rgba(0, 0, 0, 0.87)' }}>
                            Line follower
                        </Button>
                    </Toolbar>
                </AppBar>
                {this.state.mode === MANUAL_CONTROL && <Control />}
                {this.state.mode === LINE_FOLLOWER && <LineFollow />}
            </div>
        );
    }
}

export default Main;
