import React from 'react';
import io from 'socket.io-client';
import { Card, CardHeader, Typography, Slider, CardContent, Paper, Button } from '@material-ui/core/';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import api from '../api';
import config from '../config';

const RELEASE = 0;
const FORWARD = 1;
const BACKWARD = 2;

class Control extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            speed: 0,
            angle: 0,
            direction: RELEASE,
            distance: 0,
        };

        this.socket = io.connect(config.apiUrl);
        this.socket.on('distance', (distance) => {
            this.setState({ distance });
        });
    }

    componentWillMount() {
        window.addEventListener('keydown', (event) => this.downHandler(event));
        window.addEventListener('keyup', (event) => this.upHandler(event));
    }

    setSpeed(speed) {
        if (speed !== this.state.speed) {
            this.setState({ speed });
            api.post('/setSpeed', { speed: speed * 25.5 });
        }
    }

    setAngle(angle) {
        if (angle !== this.state.angle) {
            this.setState({ angle });
            api.post('/setAngle', { angle });
        }
    }

    setDirection(direction) {
        this.setState({ direction });
        api.post('/setDirection', { direction });
    }

    rotateInPlace(direction) {
        api.post('/rotateInPlace', { direction });
    }

    avoidObstacle(direction) {
        api.post('/avoid', { direction });
    }

    downHandler(event) {
        if (!event.repeat) {
            const key = event.key.toLowerCase();
            switch (key) {
                case 'w': {
                    this.setDirection(FORWARD);
                    break;
                }
                case 's': {
                    this.setDirection(BACKWARD);
                    break;
                }
                case 'a': {
                    if (this.state.direction === FORWARD || this.state.direction === BACKWARD) {
                        this.setAngle(20);
                    } else if (this.state.direction === RELEASE) {
                        this.rotateInPlace('left');
                    }
                    break;
                }
                case 'd': {
                    if (this.state.direction === FORWARD || this.state.direction === BACKWARD) {
                        this.setAngle(180);
                    } else if (this.state.direction === RELEASE) {
                        this.rotateInPlace('right');
                    }
                    break;
                }
                default:
            }
        }
    }

    upHandler(event) {
        if (!event.repeat) {
            const key = event.key.toLowerCase();
            switch (key) {
                case 'w':
                case 's': {
                    this.setDirection(RELEASE);
                    break;
                }
                case 'a':
                case 'd': {
                    this.setDirection(this.state.direction);
                    this.setAngle(100);
                    break;
                }
                default:
            }
        }
    }

    getColorForPercentage(pct) {
        const percentColors = [
            { pct: 0.0, color: { r: 0xff, g: 0x00, b: 0 } },
            { pct: 0.3, color: { r: 0xff, g: 0xff, b: 0 } },
            { pct: 1.0, color: { r: 0x00, g: 160, b: 0 } },
        ];

        let i;
        for (i = 1; i < percentColors.length - 1; i++) {
            if (pct < percentColors[i].pct) {
                break;
            }
        }

        const lower = percentColors[i - 1];
        const upper = percentColors[i];
        const range = upper.pct - lower.pct;
        const rangePct = (pct - lower.pct) / range;
        const pctLower = 1 - rangePct;
        const pctUpper = rangePct;
        const color = {
            r: Math.floor(lower.color.r * pctLower + upper.color.r * pctUpper),
            g: Math.floor(lower.color.g * pctLower + upper.color.g * pctUpper),
            b: Math.floor(lower.color.b * pctLower + upper.color.b * pctUpper),
        };

        return 'rgb(' + [color.r, color.g, color.b].join(',') + ')';
    }

    render() {
        return (
            <div>
                <Card style={{ maxWidth: 400, margin: 6 }}>
                    <CardHeader subheader="Robot control"></CardHeader>
                    <CardContent>
                        <Typography id="discrete-slider" gutterBottom style={{ marginBottom: 50 }}>
                            Speed
                        </Typography>

                        <div style={{ marginLeft: 25, marginRight: 25 }}>
                            <Slider
                                defaultValue={0}
                                aria-labelledby="discrete-slider"
                                step={1}
                                marks={[{ value: 0, label: 'Stopped' }, { value: 10, label: 'Max' }]}
                                track={false}
                                valueLabelDisplay="on"
                                min={0}
                                max={10}
                                onChangeCommitted={(_, value) => this.setSpeed(value)}
                            />
                        </div>
                    </CardContent>

                    <CardContent style={{ paddingBottom: 0 }}>
                        <Typography gutterBottom style={{ marginBottom: 10 }}>
                            Distance ahead
                        </Typography>

                        {this.state.distance !== 0 ? (
                            <Paper
                                style={{
                                    backgroundColor: this.getColorForPercentage(
                                        Math.min(1.0, this.state.distance / 100)
                                    ),
                                }}>
                                <Typography style={{ textAlign: 'center', padding: 5 }}>
                                    {this.state.distance} cm
                                </Typography>
                            </Paper>
                        ) : (
                            <Paper>
                                <Typography style={{ textAlign: 'center', padding: 5 }}>No information</Typography>
                            </Paper>
                        )}
                    </CardContent>

                    <CardContent>
                        <Typography gutterBottom style={{ marginBottom: 40 }}>
                            Control
                        </Typography>

                        <Typography style={{ textAlign: 'center' }} gutterBottom>
                            Use <b>WASD</b> to control the robot.
                        </Typography>
                    </CardContent>

                    <CardContent>
                        <Typography gutterBottom style={{ marginBottom: 10 }}>
                            Obstacle avoidance
                        </Typography>

                        <Button
                            variant="contained"
                            disabled={this.state.distance > 30}
                            startIcon={<ChevronLeftIcon />}
                            onClick={() => this.avoidObstacle('left')}>
                            Avoid left
                        </Button>

                        <Button
                            variant="contained"
                            disabled={this.state.distance > 30}
                            endIcon={<ChevronRightIcon />}
                            style={{ float: 'right' }}
                            onClick={() => this.avoidObstacle('right')}>
                            Avoid right
                        </Button>
                    </CardContent>
                </Card>
            </div>
        );
    }
}

export default Control;
