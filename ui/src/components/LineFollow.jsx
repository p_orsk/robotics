import React from 'react';
import {
    Card,
    CardHeader,
    CardContent,
    Button,
    Dialog,
    DialogTitle,
    LinearProgress,
    Typography,
} from '@material-ui/core/';

import api from '../api';

class LineFollow extends React.Component {
    constructor() {
        super();

        this.state = {
            calibrating: false,
        };
    }

    calibrateLightSensors() {
        this.setState({ calibrating: true });
        api.post('/calibrateLightSensors').then(() => {
            setTimeout(() => {
                this.setState({ calibrating: false });
            }, 15000);
        });
    }

    startFollowing() {
        api.post('/startFollowing');
    }

    stopFollowing() {
        api.post('/stopFollowing');
    }

    render() {
        return (
            <>
                <Card style={{ maxWidth: 400, margin: 6 }}>
                    <CardHeader subheader="Line follower"></CardHeader>
                    <CardContent style={{ justifyContent: 'center', display: 'flex' }}>
                        <Button onClick={() => this.calibrateLightSensors()} variant="contained">
                            Calibrate light sensors
                        </Button>
                    </CardContent>
                    <CardContent>
                        <Button
                            onClick={() => this.startFollowing()}
                            variant="contained"
                            style={{ backgroundColor: 'green', color: 'white' }}>
                            Start following
                        </Button>
                        <Button
                            onClick={() => this.stopFollowing()}
                            style={{ marginLeft: 20, backgroundColor: 'red', color: 'white' }}
                            variant="contained">
                            Stop following
                        </Button>
                    </CardContent>
                </Card>
                <Dialog open={this.state.calibrating}>
                    <DialogTitle>Calibrating light sensors</DialogTitle>
                    <Typography style={{ textAlign: 'center', marginTop: 20 }}>Please wait ...</Typography>
                    <LinearProgress style={{ margin: 20 }}></LinearProgress>
                </Dialog>
            </>
        );
    }
}

export default LineFollow;
